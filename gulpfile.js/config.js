var paths = require('./paths')

module.exports = {
  browserSync: {
    server: {
      baseDir: paths.build
    },
    open: false,
    logFileChanges: false,
    notify: false,
    online: true
  },

  pretty: {
    debug: false,
    indent_char: ' ',
    indent_size: 1,
    html: {
      unformatted: ['sub', 'sup', 'b', 'i', 'u']
    }
  },

  cleanCSS: {
    keepSpecialComments: 0
  },

  combineMq: {
    beautify: false
  },

  unCSS: {
    html: [paths.build + '/*.html'],
    ignore: [/.js/]
  }
}
