/*
  ilkome gulp light
  Version 3.6.1

  Ilya Komichev
  https://ilko.me
  https://github.com/ilkome/gulp
*/


// Modules
// =================================================================================================
var gulp = require('gulp')
var paths = require('./paths')
var watch = require('gulp-watch')
var requireDir = require('require-dir')
var runSequence = require('run-sequence')
var gutil = require('gulp-util')


// Require all tasks from gulpfile.js/tasks
// =================================================================================================
requireDir('./tasks')


// Default task
// =================================================================================================
gulp.task('default', function(done) {
  runSequence(
    ['clean'],
    ['images', 'html-pages', 'js', 'less', 'static', 'staticAtomsCSS', 'staticAtomsJS'],
    ['browserSync'],
    ['watcher'],
    done
  )
})


// CSS clean task
// ============================================
gulp.task('css-clean', function(done) {
  runSequence('html-pages', 'cssClean', done)
})


// Watcher
// =================================================================================================
gulp.task('watcher', function() {
  // Static
  // ============================================
  watch(paths.static.input, function() {
    gulp.start('static')
  })

  watch(paths.staticAtomsCSS.input, function() {
    gulp.start('staticAtomsCSS')
  })

  watch(paths.staticAtomsJS.input, function() {
    gulp.start('staticAtomsJS')
  })

  // Images
  // ============================================
  watch(paths.images.input, function() {
    gulp.start('images')
  })

  // Jade
  // ============================================
  watch(paths.jade.pages, function() {
    runSequence('html-pages', 'browserSyncReload')
  })
  watch(paths.jade.atoms, function() {
    runSequence('html-atoms', 'browserSyncReload')
  })

  // Styles
  // ============================================
  watch(paths.less.input, function() {
    gulp.start('less')
  })

  // JavaScript
  // ============================================
  watch(paths.js.input, function() {
    gulp.start('js')
  })
})
