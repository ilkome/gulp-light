var app = './app'
var build = './build'

module.exports = {
  app: app,
  build: build,

  css: {
    inputClean: [
      build + '/css/*.css'
    ],
    output: build + '/css'
  },

  static: {
    input: [
      app + '/static/**/*',
      '!' + app + '/static/img'
    ]
  },

  staticAtomsCSS: {
    input: app + '/atoms/**/static/css/*.css'
  },

  staticAtomsJS: {
    input: app + '/atoms/**/static/js/*.js'
  },

  images: {
    input: [
      app + '/static/img/**/*.+(jpg|png|gif|svg)',
      app + '/atoms/**/img/**/*.+(jpg|png|gif|svg)'
    ],
    output: build + '/img'
  },

  jade: {
    pages: app + '/pages/*.jade',
    atoms: [
      app + '/atoms/**/*.jade',
      app + '/layout/**/*.jade'
    ]
  },

  js: {
    input: [
      app + '/atoms/**/*.js',
      '!' + app + '/atoms/**/static/js/**/*.js'
    ],
    output: build + '/js'
  },

  layout: app + '/layout',

  pages: {
    input: app + '/pages/*.jade',
    folder: app + '/pages'
  },

  less: {
    input: [
      app + '/less/**/*.less',
      app + '/atoms/**/*.less'
    ],
    entry: app + '/less/index.less',
    output: build + '/css'
  }
}
