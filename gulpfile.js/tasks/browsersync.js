var config = require('../config')
var gulp = require('gulp')
var browserSync = require('browser-sync')


// BrowserSync
// =================================================================================================
gulp.task('browserSync', function() {
  return browserSync(config.browserSync)
})

gulp.task('browserSyncReload', function() {
  return browserSync.reload()
})
