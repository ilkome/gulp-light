var gulp = require('gulp')
var paths = require('../paths')
var del = require('del')


// Clean build folder
// =================================================================================================
gulp.task('clean', function(cb) {
  return del([paths.build + '/**/*'], cb)
})
