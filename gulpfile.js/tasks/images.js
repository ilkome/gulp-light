var gulp = require('gulp')
var paths = require('../paths')
var browserSync = require('browser-sync')
var gutil = require('gulp-util')
var debug = require('gulp-debug')
var plumber = require('gulp-plumber')
var changed = require('gulp-changed')
var flatten = require('gulp-flatten')
var imagemin = require('gulp-imagemin')
var pngquant = require('imagemin-pngquant')
var imageResize = require('gulp-image-resize')


// Minify images
// =================================================================================================
gulp.task('images', function() {
  return gulp.src(paths.images.input)

    .pipe(plumber(
      function(error) {
        gutil.log(gutil.colors.red('images error:'), error.message)
      }
    ))

    // Pass only unchanged files
    .pipe(changed(paths.images.output))

    .pipe(debug({ title: 'images:' }))

    // Remove structure of folders
    .pipe(flatten())

    // Minify
    .pipe(imagemin({
      progressive: true,
      svgoPlugins: [
          { removeViewBox: false },
          { cleanupIDs: true }
      ],
      use: [pngquant()]
    }))

    .pipe(gulp.dest(paths.images.output))
    .pipe(browserSync.stream())
})

// Crop images
// =================================================================================================
gulp.task('images-crop', function () {
  gulp.src(paths.images.input)
    .pipe(flatten())
    .pipe(imageResize({
      width : 333,
      height : 32,
      crop : true,
      upscale : true
    }))
    .pipe(gulp.dest(paths.images.output))
})
