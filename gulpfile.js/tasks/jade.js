var gulp = require('gulp')
var paths = require('../paths')
var config = require('../config')
var browserSync = require('browser-sync')
var gutil = require('gulp-util')
var debug = require('gulp-debug')
var plumber = require('gulp-plumber')
var changed = require('gulp-changed')
var jade = require('gulp-jade')
var jadeGlobbing = require('gulp-jade-globbing')
var prettify = require('gulp-jsbeautifier')


// Compile pages
// =================================================================================================
gulp.task('html-pages', function() {
  return gulp.src(paths.pages.input)

    .pipe(plumber(
      function(error) {
        gutil.log(gutil.colors.red('jade error:'), error.message)
      }
    ))
    .pipe(debug({ title: 'jade:' }))

    .pipe(changed(paths.build, { extension: '.html' }))
    .pipe(jadeGlobbing())
    .pipe(jade())
    .pipe(prettify(config.pretty))

    .pipe(gulp.dest(paths.build))
})


// Compile atoms
// =================================================================================================
gulp.task('html-atoms', function() {
  return gulp.src(paths.pages.input)

    .pipe(plumber(
      function(error) {
        gutil.log(gutil.colors.red('jade error:'), error.message)
      }
    ))

    .pipe(debug({ title: 'jade:' }))

    .pipe(jadeGlobbing())
    .pipe(jade())

    // Prettify
    .pipe(prettify(config.pretty))

    .pipe(gulp.dest(paths.build))
})
