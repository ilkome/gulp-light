var gulp = require('gulp')
var paths = require('../paths')
var browserSync = require('browser-sync')
var debug = require('gulp-debug')
var gutil = require('gulp-util')
var plumber = require('gulp-plumber')
var changed = require('gulp-changed')
var concat = require('gulp-concat')
var sourcemaps = require('gulp-sourcemaps')


// Combine JavaScript
// ===============================================================================================
gulp.task('js', function() {
  return gulp.src(paths.js.input)

    .pipe(plumber(function(error) {
      gutil.log(gutil.colors.red('js error:'), error.message)
    }))

    // Pass only unchanged files
    .pipe(changed(paths.js.output, { extension: '.js' }))

    // Source map
    .pipe(sourcemaps.init())
      .pipe(concat('app.js'))
    .pipe(sourcemaps.write('./'))

    .pipe(debug({ title: 'js:' }))

    .pipe(gulp.dest(paths.js.output))
    .pipe(browserSync.stream())
})
