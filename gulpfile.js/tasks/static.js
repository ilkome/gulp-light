var gulp = require('gulp')
var paths = require('../paths')
var browserSync = require('browser-sync')
var gutil = require('gulp-util')
var debug = require('gulp-debug')
var flatten = require('gulp-flatten')
var plumber = require('gulp-plumber')
var changed = require('gulp-changed')


// Сopy everything to build folder
// =================================================================================================
gulp.task('static', function() {
  return gulp.src(paths.static.input)

    .pipe(plumber(function(error) {
      gutil.log(gutil.colors.red('static error:'), error.message)
    }))

    // Pass only unchanged files
    .pipe(changed(paths.build))

    .pipe(debug({ title: 'static:' }))

    .pipe(gulp.dest(paths.build))
    .pipe(browserSync.stream())
})


gulp.task('staticAtomsCSS', function() {
  return gulp.src(paths.staticAtomsCSS.input)

    .pipe(plumber(function(error) {
      gutil.log(gutil.colors.red('staticAtomsCSS error:'), error.message)
    }))

    // Pass only unchanged files
    .pipe(changed(paths.build))

    // Remove structure of folders
    .pipe(flatten())

    .pipe(debug({ title: 'static:' }))

    .pipe(gulp.dest(paths.css.output))
    .pipe(browserSync.stream())
})


gulp.task('staticAtomsJS', function() {
  return gulp.src(paths.staticAtomsJS.input)

    .pipe(plumber(function(error) {
      gutil.log(gutil.colors.red('staticAtomsJS error:'), error.message)
    }))

    // Pass only unchanged files
    .pipe(changed(paths.build))

    // Remove structure of folders
    .pipe(flatten())

    .pipe(debug({ title: 'static:' }))

    .pipe(gulp.dest(paths.build + '/js/libs'))
    .pipe(browserSync.stream())
})
