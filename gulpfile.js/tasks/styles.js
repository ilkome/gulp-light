var gulp = require('gulp')
var paths = require('../paths')
var config = require('../config')
var browserSync = require('browser-sync')
var gutil = require('gulp-util')
var debug = require('gulp-debug')
var plumber = require('gulp-plumber')
var less = require('gulp-less')
var prefix = require('gulp-autoprefixer')
var cleanCSS = require('gulp-clean-css')
var combineMq = require('gulp-combine-mq')
var unCSS = require('gulp-uncss')
var prettify = require('gulp-jsbeautifier')
var rename = require('gulp-rename')


// Compile less
// =================================================================================================
gulp.task('less', function(done) {
  return gulp.src(paths.less.entry)

    .pipe(plumber(function(error) {
      gutil.log(gutil.colors.red('less error:'), error.message)
      done()
    }))

    // Show name of file in pipe
    .pipe(debug({ title: 'less:' }))

    // Less
    .pipe(less({
      plugins: [require('less-plugin-glob')]
    }))

    // Rename
    .pipe(rename({
      basename: 'styles'
    }))

    // Autoprefixer
    .pipe(prefix('last 4 version', 'ie 11'))

    .pipe(gulp.dest(paths.less.output))
    .pipe(browserSync.stream({ match: '**/*.css' }))
})


// Minify CSS in build folder
// =================================================================================================
gulp.task('cssClean', function() {
  return gulp.src(paths.css.inputClean)

    .pipe(plumber(function(error) {
      gutil.log(gutil.colors.red('cssClean error:'), error.message)
    }))

    // Show name of file in pipe
    .pipe(debug({ title: 'css clean:' }))

    // Remove unused styles
    .pipe(unCSS(config.unCSS))

    // Combine Media queries
    .pipe(combineMq(config.combineMq))

    // Minify
    .pipe(cleanCSS(config.cleanCSS))

    // Autoprefixer
    .pipe(prefix('last 4 version', 'ie 11'))

    // Prettify
    // .pipe(prettify(config.pretty))

    .pipe(gulp.dest(paths.css.output))
    .pipe(browserSync.stream({ match: '**/*.css' }))
})
