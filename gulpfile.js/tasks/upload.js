var gulp = require('gulp')
var paths = require('../paths')
var configFTP = require('../config-ftp')
var gutil = require('gulp-util')
var ftp = require('vinyl-ftp')
var plumber = require('gulp-plumber')

var conn = ftp.create({
  host: configFTP.host,
  user: configFTP.user,
  password: configFTP.password,
  parallel: 10,
  log: gutil.log
})


// Upload files to server
// =================================================================================================
gulp.task('upload', function() {
  return gulp.src(paths.build + '/**/*', { base: paths.build, buffer: false })

    .pipe(plumber(function(error) {
      gutil.log(gutil.colors.red('upload error:'), error.message)
    }))

    .pipe(conn.newer('/'))
    .pipe(conn.dest(configFTP.dest))
})
